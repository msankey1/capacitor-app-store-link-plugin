# capacitor-app-store-link-plugin

plugin that will navigate to the play store if native, or show a qr code if web

## Install

```bash
npm install capacitor-app-store-link-plugin
npx cap sync
```

## API

<docgen-index>

* [`showLink(...)`](#showlink)

</docgen-index>

<docgen-api>
<!--Update the source file JSDoc comments and rerun docgen to update the docs below-->

### showLink(...)

```typescript
showLink(args: { url: string; }) => Promise<boolean>
```

shows the qr code or if on native, redirects to app store

| Param      | Type                          |
| ---------- | ----------------------------- |
| **`args`** | <code>{ url: string; }</code> |

**Returns:** <code>Promise&lt;boolean&gt;</code>

--------------------

</docgen-api>
