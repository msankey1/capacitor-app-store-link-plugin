package com.capacitor.plugins.appstorelink;

import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import androidx.appcompat.app.AppCompatActivity;
public class AppStoreLink {
    private AppCompatActivity activity;
    public AppStoreLink(AppCompatActivity activity) {
        this.activity = activity;
    }
    public void showLink(String url) {
        Log.i("plugin-demo", "url" + url);
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        activity.startActivity(browserIntent);
    }
}