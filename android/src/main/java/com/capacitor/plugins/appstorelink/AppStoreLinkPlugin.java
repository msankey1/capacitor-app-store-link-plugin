package com.capacitor.plugins.appstorelink;

import android.util.Log;
import com.getcapacitor.Plugin;
import com.getcapacitor.PluginCall;
import com.getcapacitor.PluginMethod;
import com.getcapacitor.annotation.CapacitorPlugin;

@CapacitorPlugin(name = "AppStoreLink")
public class AppStoreLinkPlugin extends Plugin {
    private AppStoreLink implementation;
    @Override
    public void load() {
        implementation = new AppStoreLink(getActivity());
    }

    @PluginMethod()
    public void showLink(PluginCall call) {
        String url = call.getString("url");
        Log.i("plugin-demo", "worked");
        implementation.showLink(url);
        call.resolve();
    }
}