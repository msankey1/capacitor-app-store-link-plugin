import { WebPlugin } from "@capacitor/core";

import type { AppStoreLinkPlugin } from "./definitions";


export class AppStoreLinkWeb extends WebPlugin implements AppStoreLinkPlugin {
  constructor()  {
    super()
  }

  async showLink(): Promise<boolean> {
    return true
  }
}