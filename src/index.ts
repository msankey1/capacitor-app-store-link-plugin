import { registerPlugin } from "@capacitor/core";

import type { AppStoreLinkPlugin } from "./definitions";

const AppStoreLink = registerPlugin<AppStoreLinkPlugin>(
  'AppStoreLink', {
    web: () => import('./web').then(m => new m.AppStoreLinkWeb())
  }
)

export * from './definitions'
export { AppStoreLink }