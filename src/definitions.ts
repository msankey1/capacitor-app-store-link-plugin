export interface AppStoreLinkPlugin {
  /**
   * shows the qr code or if on native, redirects to app store
   */
  showLink(args: {url: string}): Promise<boolean>
}

// on web, open a modal